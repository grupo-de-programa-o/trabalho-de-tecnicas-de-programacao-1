>>>>>>>>>>>>>>>LEIA-ME<<<<<<<<<<<<<<<<

Este projeto foi desenvolvido por Bruno Britto, Carlos Daniel e Eduardo Furtado Ramos.
O projeto tem como finalidade facilitar cálculos referentes a rentabilidade de investimentos
e de atraso de dívidas. Para isso foi elaborado um aplicativo em formato python para facilitar
essa tarefa. O programa é bem intuitivo, sem muita dificuldade para ser executado.
A versão usada foi o python 3.10 e desenvolvida no IDlE Pycharm 2022.

