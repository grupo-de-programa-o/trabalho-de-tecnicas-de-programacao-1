#Código desenvolvido por Eduardo Furtado Ramos
#Colaboradores: Bruno Brito, Carlos Daniel e Eduardo Furtado Ramos
#Projeto de técnicas de programação
from time import sleep
print("=" * 20, "Olá, seja bem vindo!", "=" * 20)
sleep(1)
print("Para começarmos, digite o seu nome abaixo:")
nome = str(input("").strip())
sleep(1)
print("Muito prazer, {}!".format(nome))
sleep(2)
print("Este é um programa que irá calcular seus investimentos!")
sleep(2)
print("Responda com SIM ou NÃO")
sleep(2)
resposta1 = str(input("Deseja prosseguir?\n")).strip().lower()
if resposta1 == "sim":
    print("OK, vamos continuar!")
    sleep(1)
    print("Ok, vamos calcular seus investimentos, {}!".format(nome))
    sleep(2)
    escolha = int(input("O que você quer calcular?\n[1]Rentabilidade\n[2]Conta atrasada\n"))
    # Aqui o usuário poderá escolher 2 opções, das quais estarão incluidas algumas perguntas...
    if escolha == 1:
        print("Ok! Iremos calcular a rentabilidade do seu investimento, {}!".format(nome))
        sleep(1)
        capital = int(input("Digite o valor do seu capital aplicado:\nR$"))
        sleep(1)
        juros = float(input("Digite o valor do juros a.a do investimento aplicado(apenas número):\n"))
        meses = int(input("Em quantos meses você deseja retornar esse dinheiro?\n"))
        rentabilidade = capital + ((juros / 100) / 12) * meses * capital
        sleep(2)
        print("Calculando...")
        sleep(3)
        print("A sua rentabilidade sobre o investimento é aproximadamente:\n R${:.2f}".format(rentabilidade))
        input("Pressione enter para sair:")
    else:
        print("Ok! Iremos calcular sua conta atrasada,{}!".format(nome))
        sleep(2)
        valor = int(input("Para começarmos, digite o valor original da sua conta:\nR$"))
        sleep(2)
        dias = int(input("Digite a quantidade de dias de atraso:\n"))
        sleep(2)
        juros = int(input("Digite o valor do juros a.m da conta:\n"))
        sleep(2)
        divida = valor + (((juros / 100) / 30) * dias) * valor
        sleep(1)
        print("Calculando...")
        sleep(2)
        print("Sua divívida atual é R${:.2f}!".format(divida))
        input("Pressione enter para sair:")
else:
    print("Tudo bem, aguardo você por aqui, {}!".format(nome))
    exit()
